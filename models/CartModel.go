package models

import "online-store/structs"

type Cart struct {
	ID         int              `json:"ID" gorm:"column:id;primaryKey;autoIncrement"`
	ItemQty    int              `json:"ItemQty" gorm:"column:itemQty"`
	Discount   int              `json:"Discount" gorm:"column:discount"`
	TotalPrice int              `json:"TotalPrice" gorm:"column:totalPrice"`
	Status     int              `json:"Status" gorm:"column:status"`
	UserId     int              `json:"UserId" gorm:"column:userId"`
	CreatedAt  structs.NullTime `json:"CreatedAt" gorm:"column:createdAt"`
	UpdatedAt  structs.NullTime `json:"UpdatedAt" gorm:"column:updatedAt"`
	DeletedAt  structs.NullTime `json:"DeletedAt" gorm:"column:deletedAt"`
}

type Carts []Cart

func (Cart) TableName() string {
	return "carts"
}
