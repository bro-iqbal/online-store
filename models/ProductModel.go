package models

import "online-store/structs"

type Product struct {
	ID          int              `json:"ID" gorm:"column:id;primaryKey;autoIncrement"`
	Name        string           `json:"Name" gorm:"column:name"`
	Description string           `json:"Description" gorm:"column:description"`
	Price       int              `json:"Price" gorm:"column:price"`
	Stock       int              `json:"Stock" gorm:"column:stock"`
	Discount    int              `json:"Discount" gorm:"column:discount"`
	EventId     int              `json:"EventId" gorm:"column:eventId"`
	EventStock  int              `json:"EventStock" gorm:"column:eventStock"`
	CategoryId  int              `json:"CategoryId" gorm:"column:categoryId"`
	Status      int              `json:"Status" gorm:"column:status"`
	CreatedBy   int              `json:"CreatedBy" gorm:"column:createdBy"`
	CreatedAt   structs.NullTime `json:"CreatedAt" gorm:"<-:create;column:createdAt"`
	UpdatedBy   int              `json:"UpdatedBy" gorm:"column:updatedBy"`
	UpdatedAt   structs.NullTime `json:"UpdatedAt" gorm:"column:updatedAt"`
	DeletedBy   int              `json:"DeletedBy" gorm:"column:deletedBy"`
	DeletedAt   structs.NullTime `json:"DeletedAt" gorm:"column:deletedAt"`
}

type Products []Product

func (Product) TableName() string {
	return "products"
}
