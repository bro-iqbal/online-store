package models

import "online-store/structs"

type Order struct {
	ID            int              `json:"ID" gorm:"column:id;primaryKey;autoIncrement"`
	ItemQty       int              `json:"ItemQty" gorm:"column:itemQty"`
	SubTotal      int              `json:"SubTotal" gorm:"column:subTotal"`
	Tax           int              `json:"Tax" gorm:"column:tax"`
	ShippingPrice int              `json:"ShippingPrice" gorm:"column:shippingPrice"`
	ShippingId    int              `json:"ShippingId" gorm:"column:shippingId"`
	TotalPrice    int              `json:"TotalPrice" gorm:"column:totalPrice"`
	Promo         int              `json:"Promo" gorm:"column:promo"`
	Discount      int              `json:"Discount" gorm:"column:discount"`
	GrandTotal    int              `json:"GrandTotal" gorm:"column:grandTotal"`
	Status        int              `json:"Status" gorm:"column:status"`
	UserId        int              `json:"UserId" gorm:"column:userId"`
	CreatedAt     structs.NullTime `json:"CreatedAt" gorm:"column:createdAt"`
	UpdatedAt     structs.NullTime `json:"UpdatedAt" gorm:"column:updatedAt"`
	DeletedAt     structs.NullTime `json:"DeletedAt" gorm:"column:deletedAt"`
}

type Orders []Order

func (Order) TableName() string {
	return "orders"
}
