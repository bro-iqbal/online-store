package models

import "online-store/structs"

type Event struct {
	ID          int              `json:"ID" gorm:"column:id;primaryKey;autoIncrement"`
	Name        string           `json:"Name" gorm:"column:name"`
	Description string           `json:"Description" gorm:"column:description"`
	Discount    int              `json:"Discount" gorm:"column:discount"`
	Status      int              `json:"Status" gorm:"column:status"`
	CreatedBy   int              `json:"CreatedBy" gorm:"column:createdBy"`
	CreatedAt   structs.NullTime `json:"CreatedAt" gorm:"<-:create;column:createdAt"`
	UpdatedBy   int              `json:"UpdatedBy" gorm:"column:updatedBy"`
	UpdatedAt   structs.NullTime `json:"UpdatedAt" gorm:"column:updatedAt"`
	DeletedBy   int              `json:"DeletedBy" gorm:"column:deletedBy"`
	DeletedAt   structs.NullTime `json:"DeletedAt" gorm:"column:deletedAt"`
}

type Events []Event

func (Event) TableName() string {
	return "events"
}
