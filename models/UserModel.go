package models

import "online-store/structs"

type User struct {
	ID        int              `json:"ID" gorm:"column:id;primaryKey;autoIncrement"`
	Name      string           `json:"Name" gorm:"column:name"`
	Email     string           `json:"Email" gorm:"column:email"`
	Address   string           `json:"Address" gorm:"column:address"`
	PhoneNo   string           `json:"PhoneNo" gorm:"column:phoneNo"`
	Password  string           `json:"Password" gorm:"column:password"`
	Status    int              `json:"Status" gorm:"column:status"`
	CreatedBy int              `json:"CreatedBy" gorm:"column:createdBy"`
	CreatedAt structs.NullTime `json:"CreatedAt" gorm:"<-:create;column:createdAt"`
	UpdatedBy int              `json:"UpdatedBy" gorm:"column:updatedBy"`
	UpdatedAt structs.NullTime `json:"UpdatedAt" gorm:"column:updatedAt"`
	DeletedBy int              `json:"DeletedBy" gorm:"column:deletedBy"`
	DeletedAt structs.NullTime `json:"DeletedAt" gorm:"column:deletedAt"`
}

type Users []User

func (User) TableName() string {
	return "users"
}
