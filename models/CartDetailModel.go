package models

import "online-store/structs"

type CartDetail struct {
	ID         int              `json:"ID" gorm:"column:id;primaryKey;autoIncrement"`
	CartId     int              `json:"CartId" gorm:"column:cartId"`
	ProductId  int              `json:"ProductId" gorm:"column:productId"`
	Price      int              `json:"Price" gorm:"column:price"`
	Qty        int              `json:"Qty" gorm:"column:qty"`
	Discount   int              `json:"Discount" gorm:"column:discount"`
	TotalPrice int              `json:"TotalPrice" gorm:"column:totalPrice"`
	Status     int              `json:"Status" gorm:"column:status"`
	CreatedAt  structs.NullTime `json:"CreatedAt" gorm:"column:createdAt"`
	UpdatedAt  structs.NullTime `json:"UpdatedAt" gorm:"column:updatedAt"`
	DeletedAt  structs.NullTime `json:"DeletedAt" gorm:"column:deletedAt"`
}

type CartDetails []CartDetail

func (CartDetail) TableName() string {
	return "cart_details"
}
