package models

import "online-store/structs"

type OrderDetail struct {
	ID         int              `json:"ID" gorm:"column:id;primaryKey;autoIncrement"`
	OrderId    int              `json:"OrderId" gorm:"column:orderId"`
	ProductId  int              `json:"ProductId" gorm:"column:productId"`
	Price      int              `json:"Price" gorm:"column:price"`
	Qty        int              `json:"Qty" gorm:"column:qty"`
	Discount   int              `json:"Discount" gorm:"column:discount"`
	TotalPrice int              `json:"TotalPrice" gorm:"column:totalPrice"`
	Status     int              `json:"Status" gorm:"column:status"`
	UpdatedAt  structs.NullTime `json:"UpdatedAt" gorm:"column:updatedAt"`
	CreatedAt  structs.NullTime `json:"CreatedAt" gorm:"column:createdAt"`
	DeletedAt  structs.NullTime `json:"DeletedAt" gorm:"column:deletedAt"`
}

type OrderDetails []OrderDetail

func (OrderDetail) TableName() string {
	return "order_details"
}
