package route

import (
	"log"
	"net/http"
	"online-store/controllers"
	"online-store/helpers"
	"online-store/middleware"
	"os"

	"github.com/gorilla/mux"
)

func Route() {
	router := mux.NewRouter().StrictSlash(true)

	router.NotFoundHandler = http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		helpers.HandleResponse(rw, 404, "Route not found", nil)

		return
	})

	router.MethodNotAllowedHandler = http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		helpers.HandleResponse(rw, 405, "Method not allowed", nil)

		return
	})

	v1 := "/api/v1/"

	router.HandleFunc(v1+"login", controllers.Login).Methods("POST")

	// user route
	router.HandleFunc(v1+"user/list", middleware.Auth(controllers.UserList)).Methods("GET")
	router.HandleFunc(v1+"user/detail/{id}", middleware.Auth(controllers.UserDetail)).Methods("GET")
	router.HandleFunc(v1+"user/create", middleware.Auth(controllers.UserCreate)).Methods("POST")
	router.HandleFunc(v1+"user/update/{id}", middleware.Auth(controllers.UserUpdate)).Methods("PUT")
	router.HandleFunc(v1+"user/delete/{id}", middleware.Auth(controllers.UserDelete)).Methods("DELETE")

	// product route
	router.HandleFunc(v1+"product/list", middleware.Auth(controllers.ProductList)).Methods("GET")
	router.HandleFunc(v1+"product/detail/{id}", middleware.Auth(controllers.ProductDetail)).Methods("GET")
	router.HandleFunc(v1+"product/create", middleware.Auth(controllers.ProductCreate)).Methods("POST")
	router.HandleFunc(v1+"product/update/{id}", middleware.Auth(controllers.ProductUpdate)).Methods("PUT")
	router.HandleFunc(v1+"product/delete/{id}", middleware.Auth(controllers.ProductDelete)).Methods("DELETE")

	// cart route
	router.HandleFunc(v1+"cart/user", middleware.Auth(controllers.CartUser)).Methods("GET")
	router.HandleFunc(v1+"cart/item/{id}", middleware.Auth(controllers.CartItems)).Methods("GET")
	router.HandleFunc(v1+"cart/create", middleware.Auth(controllers.CartCreate)).Methods("POST")
	router.HandleFunc(v1+"cart/update/{id}", middleware.Auth(controllers.CartUpdate)).Methods("PUT")
	router.HandleFunc(v1+"cart/delete/{cartId}/{cartItemId}", middleware.Auth(controllers.CartDelete)).Methods("DELETE")
	router.HandleFunc(v1+"cart/reset/{id}", middleware.Auth(controllers.CartReset)).Methods("DELETE")

	// event route
	router.HandleFunc(v1+"event/list", middleware.Auth(controllers.EventList)).Methods("GET")
	router.HandleFunc(v1+"event/product/{id}", middleware.Auth(controllers.EventProduct)).Methods("GET")
	router.HandleFunc(v1+"event/create", middleware.Auth(controllers.EventCreate)).Methods("POST")
	router.HandleFunc(v1+"event/update/{id}", middleware.Auth(controllers.EventUpdate)).Methods("PUT")
	router.HandleFunc(v1+"event/delete/{id}", middleware.Auth(controllers.EventDelete)).Methods("DELETE")
	router.HandleFunc(v1+"event/addproduct/{eventId}", middleware.Auth(controllers.EventAddProduct)).Methods("POST")
	router.HandleFunc(v1+"event/removeproduct/{eventId}", middleware.Auth(controllers.EventRemoveProduct)).Methods("POST")

	// order route
	router.HandleFunc(v1+"order/create", middleware.Auth(controllers.CreateOrder)).Methods("POST")
	router.HandleFunc(v1+"order/cancel", middleware.Auth(controllers.CancelOrder)).Methods("POST")

	appUrl := os.Getenv("APP_URL")

	appPort := os.Getenv("APP_PORT")

	log.Println("[*] Running at " + appUrl + ":" + appPort)

	log.Fatal(http.ListenAndServe(":"+appPort, router))
}
