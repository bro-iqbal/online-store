package main

import (
	"log"
	"online-store/controllers"
	"online-store/route"
	"os"
	"path/filepath"

	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load(filepath.Join("./", ".env"))

	if err != nil {
		log.Fatal(err)
	}

	appName := os.Getenv("APP_NAME")

	controllers.Init()

	log.Println("[*] Running the " + appName)

	route.Route()
}
