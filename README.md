# EVERMOS - ONLINE STORE TEST

## MIGRATE TABLE

Create new database called online_store, then run this command to migrate tables

```bash
migrate -path database/migrations -database "mysql://root:123123@tcp(127.0.0.1:3306)/online_store" -verbose up
```

## POSTMAN

### Documentation

You can clone the project to your local machine for running locally, or you can change the url http:localhost:8080/ to http://51.79.248.83:8080/

https://documenter.getpostman.com/view/1585979/UUy38m7p

### Collection

https://www.getpostman.com/collections/3602b832e789d46220aa

## CASE

Describe what you think happened that caused those bad reviews during our 12.12 event and why it happened.

-   Ulasan buruk muncul karena pelanggan merasa tidak puas atas pelayan yang diberikan, terlebih lagi pelanggan sudah membayar dan berharap akan mendapatkan pesanan yang diinginkan namun malah mendapatkan konfirmasi bahwa pesanan dibatalkan.

Based on your analysis, propose a solution that will prevent the incidents from occurring again.

-   Solusi atas permasalahan tersebut adalah dengan memastikan kesamaan data antara gudang dan system secara berkala.
-   Mengoptimalkan system informasi agar tidak terjadi jumlah stok negatif, seperti memvalidasi jumlah stok peritem agar tidak lebih kecil dari jumlah yang dipesan setiap ada pesanan baru.
-   Membatasi jumlah quantity per item yang akan dijual dalam event - event seperti 12.12, agar tidak terjadi pesanan yang melebihi kuota.
