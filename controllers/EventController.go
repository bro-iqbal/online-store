package controllers

import (
	"fmt"
	"log"
	"net/http"
	"online-store/helpers"
	. "online-store/models"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/thedevsaddam/govalidator"
)

func EventList(rw http.ResponseWriter, r *http.Request) {
	var events Events
	var status, name, search string

	if r.URL.Query()["Status"] != nil {
		status = r.URL.Query()["Status"][0]
	}
	if r.URL.Query()["Name"] != nil {
		name = r.URL.Query()["Name"][0]
	}
	if r.URL.Query()["Search"] != nil {
		search = r.URL.Query()["Search"][0]
	}

	var query = "status <> 2 AND deletedAt IS NULL "

	if status != "" {
		query = query + "AND status = " + fmt.Sprintf("'%v'", status)
	} else if name != "" {
		query = query + "AND name LIKE '%" + fmt.Sprintf("%v", name) + "%'"
	} else if search != "" {
		query = query + "AND name LIKE '%" + fmt.Sprintf("%v", search) + "%' " + "OR status LIKE '%" + fmt.Sprintf("%v", search) + "%'"
	}

	result := db.Where(query).Find(&events)

	if result.Error != nil {
		helpers.HandleResponse(rw, 400, "Get event list failed", nil)

		return
	}

	helpers.HandleResponse(rw, 200, "Get event list successful", events)

	return
}

func EventProduct(rw http.ResponseWriter, r *http.Request) {
	var product Product

	id, _ := strconv.Atoi(mux.Vars(r)["id"])

	result := db.Where("eventId = ? AND status <> 2 AND deletedAt IS NULL", id).Find(&product)

	if result.Error != nil {
		helpers.HandleResponse(rw, 400, "Get event products failed", nil)

		return
	}

	helpers.HandleResponse(rw, 200, "Get event products successful", product)

	return
}

func EventCreate(rw http.ResponseWriter, r *http.Request) {
	userId, _ := strconv.Atoi(fmt.Sprintf("%v", helpers.GetAuthorizationTokenValue(r, "UserId")))

	var event Event

	validator := govalidator.New(govalidator.Options{
		Request: r,
		Rules: govalidator.MapData{
			"Name":        []string{"required: The name field is required"},
			"Description": []string{"required: The description field is required"},
			"Discount":    []string{"numeric"},
		},
		RequiredDefault: true,
	}).Validate()

	if len(validator) > 0 {
		helpers.HandleResponse(rw, 400, "Validation error", validator)

		return
	}

	discount, _ := strconv.Atoi(r.FormValue("Discount"))

	event.Name = r.FormValue("Name")
	event.Description = r.FormValue("Description")
	event.Discount = discount
	event.Status = 1
	event.CreatedBy = userId

	result := db.Create(&event)

	if result.Error != nil {
		helpers.HandleResponse(rw, 400, "Create event failed", nil)

		return
	}

	db.Order("id DESC").First(&event)

	helpers.HandleResponse(rw, 200, "Create event successful", event)

	return
}

func EventUpdate(rw http.ResponseWriter, r *http.Request) {
	userId, _ := strconv.Atoi(fmt.Sprintf("%v", helpers.GetAuthorizationTokenValue(r, "UserId")))
	var event Event

	validator := govalidator.New(govalidator.Options{
		Request: r,
		Rules: govalidator.MapData{

			"Name":        []string{},
			"Description": []string{},
			"Discount":    []string{"numeric"},
		},
		RequiredDefault: true,
	}).Validate()

	if len(validator) > 0 {
		helpers.HandleResponse(rw, 400, "Validation error", validator)

		return
	}

	id, _ := strconv.Atoi(mux.Vars(r)["id"])

	discount, _ := strconv.Atoi(r.FormValue("Discount"))

	event.ID = id
	event.Name = r.FormValue("Name")
	event.Description = r.FormValue("Description")
	event.Discount = discount
	event.Status = 1
	event.UpdatedBy = userId

	result := db.Where("id = ?", id).Model(&event).Updates(event)

	if result.Error != nil || result.RowsAffected == 0 {
		helpers.HandleResponse(rw, 400, "Update event id failed", nil)

		return
	}

	helpers.HandleResponse(rw, 200, "Update event successful", event)

	return
}

func EventDelete(rw http.ResponseWriter, r *http.Request) {
	var event Event
	var product Product

	id, _ := strconv.Atoi(mux.Vars(r)["id"])

	product.EventId = 0

	db.Where("eventId = ?", id).Model(&product).Updates(product)

	event.Status = 2

	db.Where("id = ?", id).Model(&event).Updates(event)

	result := db.Model(&event).Where("id = ?", id).Delete(event)
	log.Println(result.RowsAffected)
	if result.Error != nil || result.RowsAffected == 0 {
		helpers.HandleResponse(rw, 400, "Delete event id failed", nil)

		return
	}

	helpers.HandleResponse(rw, 200, "Delete event successful", map[string]interface{}{
		"ID": id,
	})

	return
}

func EventAddProduct(rw http.ResponseWriter, r *http.Request) {
	var product Product

	eventId, _ := strconv.Atoi(mux.Vars(r)["eventId"])

	validator := govalidator.New(govalidator.Options{
		Request: r,
		Rules: govalidator.MapData{
			"ProductId":  []string{"required: The product id field is required", "numeric"},
			"EventStock": []string{"required: The event stock field is required", "numeric"},
		},
		RequiredDefault: true,
	}).Validate()

	if len(validator) > 0 {
		helpers.HandleResponse(rw, 400, "Validation error", validator)

		return
	}

	productId, _ := strconv.Atoi(r.FormValue("ProductId"))
	eventStock, _ := strconv.Atoi(r.FormValue("EventStock"))

	product.EventId = eventId
	product.EventStock = eventStock

	res := db.Where("id = ? AND status <> 2 AND stock >= ?", productId, eventStock).Model(&product).Updates(product)

	if res.Error != nil || res.RowsAffected == 0 {
		helpers.HandleResponse(rw, 400, "Add product to event failed", nil)

		return
	}

	helpers.HandleResponse(rw, 200, "Add product to event successful", map[string]interface{}{
		"ProductID":  productId,
		"EventID":    eventId,
		"EventStock": eventStock,
	})

	return
}

func EventRemoveProduct(rw http.ResponseWriter, r *http.Request) {
	validator := govalidator.New(govalidator.Options{
		Request: r,
		Rules: govalidator.MapData{
			"ProductId": []string{"required: The product id field is required", "numeric"},
		},
		RequiredDefault: true,
	}).Validate()

	if len(validator) > 0 {
		helpers.HandleResponse(rw, 400, "Validation error", validator)

		return
	}

	productId, _ := strconv.Atoi(r.FormValue("ProductId"))

	res := db.Exec("UPDATE products SET eventId = 0, eventStock = 0 WHERE id = ?", productId)

	if res.Error != nil || res.RowsAffected == 0 {
		helpers.HandleResponse(rw, 400, "Remove product from event failed", nil)

		return
	}

	helpers.HandleResponse(rw, 200, "Remove product from event successful", map[string]interface{}{
		"ProductID":  productId,
		"EventID":    0,
		"EventStock": 0,
	})

	return
}
