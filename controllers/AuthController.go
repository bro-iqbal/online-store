package controllers

import (
	"net/http"
	"online-store/helpers"
	"online-store/models"
	"os"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

func Login(rw http.ResponseWriter, r *http.Request) {
	var user models.User

	email := r.FormValue("email")
	password := r.FormValue("password")

	result := db.Where("email = ?", email).First(&user)

	if result.Error != nil {
		helpers.HandleResponse(rw, 400, "Login failed", result.Error)

		return
	}

	pwdMatch := helpers.Verify(user.Password, password)

	if email != user.Email || !pwdMatch {
		helpers.HandleResponse(rw, 400, "Login failed", result.Error)

		return
	}

	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims["UserId"] = user.ID
	claims["Exp"] = time.Now().Add(time.Hour * 1).Unix()

	t, err := token.SignedString([]byte(os.Getenv("SECRET_KEY")))

	if err != nil {
		helpers.HandleResponse(rw, 400, "Login failed", result.Error)

		return
	}

	data := map[string]interface{}{
		"token": t,
	}

	helpers.HandleResponse(rw, 200, "Login successful", data)

	return
}
