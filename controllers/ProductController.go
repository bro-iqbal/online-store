package controllers

import (
	"fmt"
	"log"
	"net/http"
	"online-store/helpers"
	"online-store/models"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/thedevsaddam/govalidator"
)

func ProductList(rw http.ResponseWriter, r *http.Request) {
	var products models.Products
	var status, name, search string

	if r.URL.Query()["Status"] != nil {
		status = r.URL.Query()["Status"][0]
	}
	if r.URL.Query()["Name"] != nil {
		name = r.URL.Query()["Name"][0]
	}
	if r.URL.Query()["Search"] != nil {
		search = r.URL.Query()["Search"][0]
	}

	var query = "status <> 2 AND deletedAt IS NULL "

	if status != "" {
		query = query + "AND status = " + fmt.Sprintf("'%v'", status)
	} else if name != "" {
		query = query + "AND name LIKE '%" + fmt.Sprintf("%v", name) + "%'"
	} else if search != "" {
		query = query + "AND name LIKE '%" + fmt.Sprintf("%v", search) + "%' " + "OR status LIKE '%" + fmt.Sprintf("%v", search) + "%'"
	}

	result := db.Where(query).Find(&products)

	if result.Error != nil {
		helpers.HandleResponse(rw, 400, "Get product list failed", nil)

		return
	}

	helpers.HandleResponse(rw, 200, "Get product list successful", products)

	return
}

func ProductDetail(rw http.ResponseWriter, r *http.Request) {
	var product models.Product

	id, _ := strconv.Atoi(mux.Vars(r)["id"])

	result := db.Where("id = ? AND status <> 2 AND deletedAt IS NULL", id).First(&product)

	if result.Error != nil {
		helpers.HandleResponse(rw, 400, "Get product by id failed", nil)

		return
	}

	helpers.HandleResponse(rw, 200, "Get product by id successful", product)

	return
}

func ProductCreate(rw http.ResponseWriter, r *http.Request) {
	userId, _ := strconv.Atoi(fmt.Sprintf("%v", helpers.GetAuthorizationTokenValue(r, "UserId")))

	var product models.Product

	validator := govalidator.New(govalidator.Options{
		Request: r,
		Rules: govalidator.MapData{
			"Name":        []string{"required: The name field is required"},
			"Description": []string{"required: The description field is required"},
			"Price":       []string{"required: The price field is required", "numeric"},
			"Stock":       []string{"numeric"},
			"Discount":    []string{"numeric"},
			"CategoryId":  []string{"required: The category id field is required", "numeric"},
		},
		RequiredDefault: true,
	}).Validate()

	if len(validator) > 0 {
		helpers.HandleResponse(rw, 400, "Validation error", validator)

		return
	}

	price, _ := strconv.Atoi(r.FormValue("Price"))
	stock, _ := strconv.Atoi(r.FormValue("Stock"))
	discount, _ := strconv.Atoi(r.FormValue("Discount"))
	categoryId, _ := strconv.Atoi(r.FormValue("CategoryId"))

	product.Name = r.FormValue("Name")
	product.Description = r.FormValue("Description")
	product.Price = price
	product.Stock = stock
	product.Discount = discount
	product.CategoryId = categoryId
	product.Status = 1
	product.CreatedBy = userId

	result := db.Create(&product)

	if result.Error != nil {
		helpers.HandleResponse(rw, 400, "Create product failed", nil)

		return
	}

	db.Order("id DESC").First(&product)

	helpers.HandleResponse(rw, 200, "Create product successful", product)

	return
}

func ProductUpdate(rw http.ResponseWriter, r *http.Request) {
	userId, _ := strconv.Atoi(fmt.Sprintf("%v", helpers.GetAuthorizationTokenValue(r, "UserId")))
	var product models.Product

	validator := govalidator.New(govalidator.Options{
		Request: r,
		Rules: govalidator.MapData{

			"Name":        []string{},
			"Description": []string{},
			"Price":       []string{"numeric"},
			"Stock":       []string{"numeric"},
			"Discount":    []string{"numeric"},
			"CategoryId":  []string{"numeric"},
		},
		RequiredDefault: true,
	}).Validate()

	if len(validator) > 0 {
		helpers.HandleResponse(rw, 400, "Validation error", validator)

		return
	}

	id, _ := strconv.Atoi(mux.Vars(r)["id"])

	price, _ := strconv.Atoi(r.FormValue("Price"))
	stock, _ := strconv.Atoi(r.FormValue("Stock"))
	discount, _ := strconv.Atoi(r.FormValue("Discount"))
	categoryId, _ := strconv.Atoi(r.FormValue("CategoryId"))

	product.ID = id
	product.Name = r.FormValue("Name")
	product.Description = r.FormValue("Description")
	product.Price = price
	product.Stock = stock
	product.Discount = discount
	product.CategoryId = categoryId
	product.Status = 1
	product.UpdatedBy = userId

	result := db.Where("id = ?", id).Model(&product).Updates(product)

	if result.Error != nil || result.RowsAffected == 0 {
		helpers.HandleResponse(rw, 400, "Update product id failed", nil)

		return
	}

	helpers.HandleResponse(rw, 200, "Update product successful", product)

	return
}

func ProductDelete(rw http.ResponseWriter, r *http.Request) {
	var product models.Product

	id, _ := strconv.Atoi(mux.Vars(r)["id"])

	product.Status = 2

	db.Where("id = ?", id).Model(&product).Updates(product)

	result := db.Model(&product).Where("id = ?", id).Delete(product)
	log.Println(result.RowsAffected)
	if result.Error != nil || result.RowsAffected == 0 {
		helpers.HandleResponse(rw, 400, "Delete product id failed", nil)

		return
	}

	helpers.HandleResponse(rw, 200, "Delete product successful", map[string]interface{}{
		"ID": id,
	})

	return
}
