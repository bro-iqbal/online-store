package controllers

import (
	"online-store/database"

	"github.com/jinzhu/gorm"
)

var db *gorm.DB

func Init() {
	db = database.Connection()

	return
}
