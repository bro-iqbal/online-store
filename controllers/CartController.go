package controllers

import (
	"fmt"
	"net/http"
	"online-store/helpers"
	"online-store/models"
	. "online-store/models"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/thedevsaddam/govalidator"
)

func CartUser(rw http.ResponseWriter, r *http.Request) {
	var cart Cart

	var query = "status <> 2 OR status <> 3 AND deletedAt IS NULL "

	result := db.Where(query).First(&cart)

	if result.Error != nil {
		helpers.HandleResponse(rw, 400, "Get cart user failed", nil)

		return
	}

	helpers.HandleResponse(rw, 200, "Get cart user successful", cart)

	return
}

func CartItems(rw http.ResponseWriter, r *http.Request) {
	var cartDetails CartDetails

	id, _ := strconv.Atoi(mux.Vars(r)["id"])

	result := db.Where("cartId = ? AND status <> 2 AND deletedAt IS NULL", id).Find(&cartDetails)

	if result.Error != nil {
		helpers.HandleResponse(rw, 400, "Get cart by id failed", nil)

		return
	}

	helpers.HandleResponse(rw, 200, "Get cart by id successful", cartDetails)

	return
}

func CartCreate(rw http.ResponseWriter, r *http.Request) {
	userId, _ := strconv.Atoi(fmt.Sprintf("%v", helpers.GetAuthorizationTokenValue(r, "UserId")))

	var cartDetail CartDetail
	var cart Cart
	var product Product

	validator := govalidator.New(govalidator.Options{
		Request: r,
		Rules: govalidator.MapData{
			"ProductId": []string{"required: The name field is required"},
			"Quantity":  []string{"required: The quantity field is required"},
		},
		RequiredDefault: true,
	}).Validate()

	if len(validator) > 0 {
		helpers.HandleResponse(rw, 400, "Validation error", validator)

		return
	}

	result := db.Where("userId = ? AND status <> 2 AND deletedAt IS NULL", userId).Find(&cart)

	if result.RowsAffected > 0 {
		CartUpdate(rw, r)
	} else {
		productId, _ := strconv.Atoi(r.FormValue("ProductId"))
		qty, _ := strconv.Atoi(r.FormValue("Quantity"))

		db.Where("id = ? AND status <> 2 AND deletedAt IS NULL", productId).First(&product)

		if qty > product.Stock {
			helpers.HandleResponse(rw, 400, "Insufficient product stock ", nil)

			return
		}

		cart.ItemQty = 1
		cart.Discount = product.Discount
		cart.TotalPrice = product.Price * qty
		cart.Status = 1
		cart.UserId = userId

		result = db.Create(&cart)

		if result.Error != nil {
			helpers.HandleResponse(rw, 400, "Create cart failed", nil)

			return
		}

		cartDetail.CartId = cart.ID
		cartDetail.ProductId = productId
		cartDetail.Price = product.Price
		cartDetail.Qty = qty
		cartDetail.Discount = product.Discount
		cartDetail.TotalPrice = product.Price * qty
		cartDetail.Status = 1

		result = db.Create(&cartDetail)

		if result.Error != nil {
			helpers.HandleResponse(rw, 400, "Create cart failed", nil)

			return
		}

		type response struct {
			Cart
			Details interface{}
		}

		resp := response{
			Cart:    cart,
			Details: cartDetail,
		}

		helpers.HandleResponse(rw, 200, "Create cart successful", resp)

		return
	}
}

func CartUpdate(rw http.ResponseWriter, r *http.Request) {
	userId, _ := strconv.Atoi(fmt.Sprintf("%v", helpers.GetAuthorizationTokenValue(r, "UserId")))

	var cartDetail models.CartDetail
	var cartDetails models.CartDetails
	var cart Cart
	var product Product

	validator := govalidator.New(govalidator.Options{
		Request: r,
		Rules: govalidator.MapData{
			"ProductId": []string{"required: The name field is required"},
			"Quantity":  []string{"required: The quantity field is required"},
		},
		RequiredDefault: true,
	}).Validate()

	if len(validator) > 0 {
		helpers.HandleResponse(rw, 400, "Validation error", validator)

		return
	}

	productId, _ := strconv.Atoi(r.FormValue("ProductId"))
	qty, _ := strconv.Atoi(r.FormValue("Quantity"))

	var cartId int

	if mux.Vars(r)["id"] != "" {
		cartId, _ = strconv.Atoi(mux.Vars(r)["id"])

		db.Where("id = ? AND status <> 2 AND deletedAt IS NULL", cartId).First(&cart)
	} else {
		db.Where("userId = ? AND status <> 2 AND deletedAt IS NULL", userId).First(&cart)

		cartId = cart.ID
	}

	db.Where("id = ? AND status <> 2 AND deletedAt IS NULL", productId).First(&product)

	if qty > product.Stock {
		helpers.HandleResponse(rw, 400, "Insufficient product stock ", nil)

		return
	}

	cart.ItemQty = cart.ItemQty + 1
	cart.TotalPrice = cart.TotalPrice + (product.Price * qty)

	db.Where("id = ?", cartId).Model(&cart).Updates(cart)

	cartDetail.CartId = cartId
	cartDetail.ProductId = productId
	cartDetail.Price = product.Price
	cartDetail.Qty = qty
	cartDetail.Discount = product.Discount
	cartDetail.TotalPrice = product.Price * qty
	cartDetail.Status = 1

	result := db.Create(&cartDetail)

	if result.Error != nil {
		helpers.HandleResponse(rw, 400, "Update cart failed", nil)

		return
	}

	db.Where("cartId = ? AND status <> 2 AND deletedAt IS NULL", cartId).Find(&cartDetails)

	type response struct {
		Cart
		Details interface{}
	}

	resp := response{
		Cart:    cart,
		Details: cartDetails,
	}

	helpers.HandleResponse(rw, 200, "Update cart successful", resp)

	return
}

func CartDelete(rw http.ResponseWriter, r *http.Request) {
	var cartDetail CartDetail
	var cart Cart

	cartItemId, _ := strconv.Atoi(mux.Vars(r)["cartItemId"])
	cartId, _ := strconv.Atoi(mux.Vars(r)["cartId"])

	db.Where("id = ?", cartId).First(&cart)
	db.Where("id = ?", cartItemId).First(&cartDetail)

	cart.ItemQty = cart.ItemQty - 1
	cart.TotalPrice = cart.TotalPrice - cartDetail.TotalPrice

	db.Where("id = ?", cartId).Model(&cart).Updates(cart)

	cartDetail.Status = 2

	db.Where("id = ?", cartItemId).Model(&cartDetail).Updates(cartDetail)

	result := db.Model(&cartDetail).Where("id = ?", cartItemId).Delete(cartDetail)

	if result.Error != nil || result.RowsAffected == 0 {
		helpers.HandleResponse(rw, 400, "Delete cart item failed", nil)

		return
	}

	helpers.HandleResponse(rw, 200, "Delete cart item successful", map[string]interface{}{
		"CartDetailID": cartItemId,
	})

	return
}

func CartReset(rw http.ResponseWriter, r *http.Request) {
	var cartDetail CartDetail
	var cart Cart

	id, _ := strconv.Atoi(mux.Vars(r)["id"])

	cart.Status = 2

	db.Where("id = ?", id).Model(&cart).Updates(cart)

	cartDetail.Status = 2

	db.Where("cartId = ?", id).Model(&cartDetail).Updates(cartDetail)

	result := db.Model(&cart).Where("id = ?", id).Delete(cart)

	if result.Error != nil || result.RowsAffected == 0 {
		helpers.HandleResponse(rw, 400, "Reset cart item failed", nil)

		return
	}

	result = db.Model(&cartDetail).Where("cartId = ?", id).Delete(cartDetail)

	if result.Error != nil || result.RowsAffected == 0 {
		helpers.HandleResponse(rw, 400, "Reset cart item failed", nil)

		return
	}

	helpers.HandleResponse(rw, 200, "Reset cart successful", map[string]interface{}{
		"CartID": id,
	})

	return
}
