package controllers

import (
	"fmt"
	"log"
	"net/http"
	"online-store/helpers"
	"online-store/models"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/thedevsaddam/govalidator"
)

func UserList(rw http.ResponseWriter, r *http.Request) {
	var users models.Users
	var status, name, search string

	if r.URL.Query()["Status"] != nil {
		status = r.URL.Query()["Status"][0]
	}
	if r.URL.Query()["Name"] != nil {
		name = r.URL.Query()["Name"][0]
	}
	if r.URL.Query()["Search"] != nil {
		search = r.URL.Query()["Search"][0]
	}

	var query = "status <> 2 AND deletedAt IS NULL "

	if status != "" {
		query = query + "AND status = " + fmt.Sprintf("'%v'", status)
	} else if name != "" {
		query = query + "AND name LIKE '%" + fmt.Sprintf("%v", name) + "%'"
	} else if search != "" {
		query = query + "AND name LIKE '%" + fmt.Sprintf("%v", search) + "%' " + "OR status LIKE '%" + fmt.Sprintf("%v", search) + "%'"
	}

	result := db.Where(query).Find(&users)

	if result.Error != nil {
		helpers.HandleResponse(rw, 400, "Get user list failed", nil)

		return
	}

	helpers.HandleResponse(rw, 200, "Get user list successful", users)

	return
}

func UserDetail(rw http.ResponseWriter, r *http.Request) {
	var user models.User

	id, _ := strconv.Atoi(mux.Vars(r)["id"])

	result := db.Where("id = ? AND status <> 2 AND deletedAt IS NULL", id).First(&user)

	if result.Error != nil {
		helpers.HandleResponse(rw, 400, "Get user by id failed", nil)

		return
	}

	helpers.HandleResponse(rw, 200, "Get user by id successful", user)

	return
}

func UserCreate(rw http.ResponseWriter, r *http.Request) {
	userId, _ := strconv.Atoi(fmt.Sprintf("%v", helpers.GetAuthorizationTokenValue(r, "UserId")))
	var user models.User

	validator := govalidator.New(govalidator.Options{
		Request: r,
		Rules: govalidator.MapData{
			"Email":    []string{"required: The email field is required"},
			"Name":     []string{"required: The name field is required"},
			"Address":  []string{"required: The address field is required"},
			"Password": []string{"required: The password field is required"},
			"PhoneNo":  []string{"required: The phone no field is required"},
		},
		RequiredDefault: true,
	}).Validate()

	if len(validator) > 0 {
		helpers.HandleResponse(rw, 400, "Validation error", validator)

		return
	}

	pass, _ := helpers.HashPassword(r.FormValue("Password"))

	user.Email = r.FormValue("Email")
	user.Name = r.FormValue("Name")
	user.Address = r.FormValue("Address")
	user.PhoneNo = r.FormValue("PhoneNo")
	user.Password = pass
	user.Status = 1
	user.CreatedBy = userId

	result := db.Create(&user)

	if result.Error != nil {
		helpers.HandleResponse(rw, 400, "Create user failed", nil)

		return
	}

	db.Order("id DESC").First(&user)

	helpers.HandleResponse(rw, 200, "Create user successful", user)

	return
}

func UserUpdate(rw http.ResponseWriter, r *http.Request) {
	userId, _ := strconv.Atoi(fmt.Sprintf("%v", helpers.GetAuthorizationTokenValue(r, "UserId")))
	var user models.User

	validator := govalidator.New(govalidator.Options{
		Request: r,
		Rules: govalidator.MapData{
			"Email":   []string{},
			"Name":    []string{},
			"Address": []string{},
			"PhoneNo": []string{},
		},
		RequiredDefault: true,
	}).Validate()

	if len(validator) > 0 {
		helpers.HandleResponse(rw, 400, "Validation error", validator)

		return
	}

	id, _ := strconv.Atoi(mux.Vars(r)["id"])

	user.ID = id
	user.Email = r.FormValue("Email")
	user.Name = r.FormValue("Name")
	user.PhoneNo = r.FormValue("PhoneNo")
	user.Address = r.FormValue("Address")
	user.UpdatedBy = userId

	result := db.Where("id = ?", id).Model(&user).Updates(user)

	if result.Error != nil || result.RowsAffected == 0 {
		helpers.HandleResponse(rw, 400, "Update user id failed", nil)

		return
	}

	helpers.HandleResponse(rw, 200, "Update user successful", user)

	return
}

func UserUpdatePassword(rw http.ResponseWriter, r *http.Request) {
	var user models.User

	validator := govalidator.New(govalidator.Options{
		Request: r,
		Rules: govalidator.MapData{
			"OldPassword":     []string{"required: The Old Password field is required"},
			"ConfirmPassword": []string{"required: The Confirm Password field is required"},
			"NewPassword":     []string{"required: The New Password field is required"},
		},
		RequiredDefault: true,
	}).Validate()

	if len(validator) > 0 {
		helpers.HandleResponse(rw, 400, "Validation error", validator)

		return
	}

	id, _ := strconv.Atoi(mux.Vars(r)["id"])

	db.Where("userid = ?", id).First(&user)

	if r.FormValue("OldPassword") == r.FormValue("confirmPassword") {
		match := helpers.Verify(r.FormValue("OldPassword"), user.Password)
		if !match {
			helpers.HandleResponse(rw, 400, "Update user password "+string(id)+" failed", nil)

			return
		}
	}

	pass, _ := helpers.HashPassword(r.FormValue("NewPassword"))

	user.Password = pass

	result := db.Where("id = ?", id).Model(&user).Updates(user)

	if result.Error != nil || result.RowsAffected == 0 {
		helpers.HandleResponse(rw, 400, "Update user password failed", result.Error)

		return
	}

	helpers.HandleResponse(rw, 200, "Update user password successful", result.Value)

	return
}

func UserDelete(rw http.ResponseWriter, r *http.Request) {
	var user models.User

	id, _ := strconv.Atoi(mux.Vars(r)["id"])

	result := db.Model(&user).Where("id = ?", id).Delete(user)
	log.Println(result.RowsAffected)
	if result.Error != nil || result.RowsAffected == 0 {
		helpers.HandleResponse(rw, 400, "Delete user id failed", nil)

		return
	}

	helpers.HandleResponse(rw, 200, "Delete user successful", map[string]interface{}{
		"ID": id,
	})

	return
}
