package controllers

import (
	"fmt"
	"net/http"
	"online-store/helpers"
	. "online-store/models"
	"strconv"
	"time"

	"github.com/thedevsaddam/govalidator"
)

func CreateOrder(rw http.ResponseWriter, r *http.Request) {
	userId, _ := strconv.Atoi(fmt.Sprintf("%v", helpers.GetAuthorizationTokenValue(r, "UserId")))

	var order Order
	var orderDetail OrderDetail
	var cart Cart
	var cartDetails CartDetails
	var product Product

	validator := govalidator.New(govalidator.Options{
		Request: r,
		Rules: govalidator.MapData{
			"ShippingPrice": []string{"required: The shipping price field is required"},
			"ShippingId":    []string{"required: The shipping id field is required"},
			"Tax":           []string{},
			"Promo":         []string{},
			"CartId":        []string{"required: The cart id field is required"},
		},
		RequiredDefault: true,
	}).Validate()

	if len(validator) > 0 {
		helpers.HandleResponse(rw, 400, "Validation error", validator)

		return
	}

	cartId, _ := strconv.Atoi(r.FormValue("CartId"))

	db.Where("id = ? AND status <> 2 OR status <> 3", cartId).First(&cart)

	if userId != cart.UserId {
		helpers.HandleResponse(rw, 400, "The user id not match", validator)

		return
	}

	db.Where("cartId = ?", cartId).Find(&cartDetails)

	tax, _ := strconv.Atoi(r.FormValue("Tax"))
	taxPercent := tax / 100
	promo, _ := strconv.Atoi(r.FormValue("Promo"))
	promoPercent := promo / 100
	shippingId, _ := strconv.Atoi(r.FormValue("ShippingId"))
	shippingPrice, _ := strconv.Atoi(r.FormValue("ShippingPrice"))

	totalPrice := cart.TotalPrice + (cart.TotalPrice * taxPercent)

	grandTotal := ((cart.TotalPrice + (cart.TotalPrice * taxPercent) + shippingPrice) - (cart.TotalPrice * promoPercent)) - cart.Discount

	order.ItemQty = cart.ItemQty
	order.SubTotal = cart.TotalPrice
	order.Tax = tax
	order.ShippingId = shippingId
	order.ShippingPrice = shippingPrice
	order.TotalPrice = totalPrice
	order.Promo = cart.TotalPrice * promoPercent
	order.Discount = cart.Discount
	order.GrandTotal = grandTotal
	order.Status = 1
	order.UserId = userId

	result := db.Create(&order)

	if result.Error != nil {
		helpers.HandleResponse(rw, 400, "Create order failed", nil)

		return
	}

	go func(orderDetail OrderDetail, cartDetails CartDetails) {
		for _, val := range cartDetails {
			orderDetail.OrderId = order.ID
			orderDetail.ProductId = val.ProductId
			orderDetail.Price = val.Price
			orderDetail.Qty = val.Qty
			orderDetail.Discount = val.Discount
			orderDetail.TotalPrice = val.TotalPrice
			orderDetail.Status = 1

			db.Create(&orderDetail)
		}
	}(orderDetail, cartDetails)
	go func(product Product, cartDetails CartDetails) {
		for _, val := range cartDetails {
			db.Where("id = ?", val.ProductId).First(&product)

			product.Stock = product.Stock - val.Qty

			db.Where("id = ?", product.ID).Model(&product).Updates(product)
		}
	}(product, cartDetails)
	go func(cart Cart, cartId int) {
		cart.Status = 3 //status ordered

		db.Where("id = ?", cartId).Model(&cart).Updates(cart)
	}(cart, cartId)

	time.Sleep(time.Second)

	helpers.HandleResponse(rw, 200, "Create order successful", order)

	return
}

func CancelOrder(rw http.ResponseWriter, r *http.Request) {
	validator := govalidator.New(govalidator.Options{
		Request: r,
		Rules: govalidator.MapData{
			"OrderId": []string{"required: The order id field is required"},
			"Reason":  []string{"required: The reason field is required"},
		},
		RequiredDefault: true,
	}).Validate()

	if len(validator) > 0 {
		helpers.HandleResponse(rw, 400, "Validation error", validator)

		return
	}

	var product Product
	var oD OrderDetails
	var order Order

	db.Where("orderId = ?", r.FormValue("OrderId")).Find(&oD)

	for _, val := range oD {
		db.Where("id = ?", val.ProductId).First(&product)

		product.Stock = product.Stock + val.Qty

		db.Where("id = ?", val.ProductId).Model(&product).Updates(product)
	}

	order.Status = 4 // status cancel

	result := db.Where("id = ?", r.FormValue("OrderId")).Model(&order).Updates(order)

	if result.Error != nil {
		helpers.HandleResponse(rw, 400, "Cancel order failed", nil)

		return
	}

	helpers.HandleResponse(rw, 200, "Cancel order successful", order)

	return
}
