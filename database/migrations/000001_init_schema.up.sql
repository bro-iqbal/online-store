CREATE TABLE `users` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `name` varchar(50) NOT NULL,
    `email` varchar(100) NOT NULL,
    `password` text NOT NULL,
    `address` text DEFAULT NULL,
    `phoneNo` varchar(15) NOT NULL,
    `status` int(2) NOT NULL,
    `api_token` varchar(255) NULL DEFAULT NULL,
    `createdBy` int NOT NULL,
    `updatedBy` int DEFAULT NULL,
    `deletedBy` int DEFAULT NULL,
    `createdAt` timestamp NOT NULL,
    `updatedAt` timestamp NULL DEFAULT NULL,
    `deletedAt` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `products` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `name` varchar(50) NOT NULL,
    `description` text NOT NULL,
    `price` int NOT NULL,
    `stock` int DEFAULT NULL,
    `discount` int DEFAULT NULL,
    `eventId` int DEFAULT NULL,
    `eventStock` int DEFAULT NULL,
    `categoryId` int NOT NULL,
    `status` int(2) NOT NULL,
    `createdBy` int NOT NULL,
    `updatedBy` int DEFAULT NULL,
    `deletedBy` int DEFAULT NULL,
    `createdAt` timestamp NOT NULL,
    `updatedAt` timestamp NULL DEFAULT NULL,
    `deletedAt` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `events` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `name` varchar(50) NOT NULL,
    `description` text NOT NULL,
    `discount` int DEFAULT NULL,
    `status` int(2) NOT NULL,
    `createdBy` int NOT NULL,
    `updatedBy` int DEFAULT NULL,
    `deletedBy` int DEFAULT NULL,
    `createdAt` timestamp NOT NULL,
    `updatedAt` timestamp NULL DEFAULT NULL,
    `deletedAt` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `carts` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `itemQty` int NOT NULL,
    `discount` int DEFAULT NULL,
    `totalPrice` int NOT NULL,
    `status` int(2) NOT NULL,
    `userId` int NOT NULL,
    `createdAt` timestamp NOT NULL,
    `updatedAt` timestamp NULL DEFAULT NULL,
    `deletedAt` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `cart_details` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `cartId` int NOT NULL,
    `productId` int DEFAULT NULL,
    `price` int DEFAULT NULL,
    `qty` int DEFAULT NULL,
    `discount` int DEFAULT NULL,
    `totalPrice` int NOT NULL,
    `status` int(2) NOT NULL,
    `createdAt` timestamp NOT NULL,
    `updatedAt` timestamp NULL DEFAULT NULL,
    `deletedAt` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `orders` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `itemQty` int NOT NULL,
    `subTotal` int DEFAULT NULL,
    `tax` int DEFAULT NULL,
    `shippingPrice` int DEFAULT NULL,
    `shippingId` int DEFAULT NULL,
    `totalPrice` int NOT NULL,
    `promo` int NOT NULL,
    `discount` int DEFAULT NULL,
    `grandTotal` int NOT NULL,
    `status` int(2) NOT NULL,
    `userId` int NOT NULL,
    `createdAt` timestamp NOT NULL,
    `updatedAt` timestamp NULL DEFAULT NULL,
    `deletedAt` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `order_details` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `orderId` int NOT NULL,
    `productId` int DEFAULT NULL,
    `price` int DEFAULT NULL,
    `qty` int DEFAULT NULL,
    `discount` int DEFAULT NULL,
    `totalPrice` int NOT NULL,
    `status` int(2) NOT NULL,
    `createdAt` timestamp NOT NULL,
    `updatedAt` timestamp NULL DEFAULT NULL,
    `deletedAt` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
);

INSERT INTO `users` VALUES (1, 'Test', 'test@mail.id', '$2a$14$MZQkVtED6j2rsE5W6pJXDuanaluZNMJy72HQVIWK9HvHB1rukMAd6', 'Jalan SM Raja No 19', '081260349944', 1, NULL, 0, 0, 0, '2021-10-01 08:40:28', '2021-10-01 08:40:28', NULL);