package structs

import "database/sql"

type NullString struct {
	sql.NullString
}

type NullInt32 struct {
	sql.NullInt32
}

type NullInt64 struct {
	sql.NullInt64
}

type NullBool struct {
	sql.NullBool
}

type NullTime struct {
	sql.NullTime
}
